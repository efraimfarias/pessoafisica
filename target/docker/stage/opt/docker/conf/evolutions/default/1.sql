# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table endereco (
  id                            bigserial not null,
  pessoa_fisica_id              bigint not null,
  cep                           varchar(255),
  logradouro                    varchar(255),
  bairro                        varchar(255),
  cidade                        varchar(255),
  uf                            varchar(255),
  constraint pk_endereco primary key (id)
);

create table pessoa_fisica (
  id                            bigserial not null,
  cpf                           bigint,
  nome                          varchar(255),
  email                         varchar(255),
  data_nascimento               timestamptz,
  data_cadastro                 timestamptz,
  telefones                     varchar[],
  constraint pk_pessoa_fisica primary key (id)
);

create index ix_endereco_pessoa_fisica_id on endereco (pessoa_fisica_id);
alter table endereco add constraint fk_endereco_pessoa_fisica_id foreign key (pessoa_fisica_id) references pessoa_fisica (id) on delete restrict on update restrict;


# --- !Downs

alter table if exists endereco drop constraint if exists fk_endereco_pessoa_fisica_id;
drop index if exists ix_endereco_pessoa_fisica_id;

drop table if exists endereco cascade;

drop table if exists pessoa_fisica cascade;

