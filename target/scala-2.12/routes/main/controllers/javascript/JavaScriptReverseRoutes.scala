// @GENERATOR:play-routes-compiler
// @SOURCE:/home/efraimfarias/samaiait/conf/routes
// @DATE:Sat Jul 06 22:44:17 BRT 2019

import play.api.routing.JavaScriptReverseRoute


import _root_.controllers.Assets.Asset
import _root_.play.libs.F

// @LINE:6
package controllers.javascript {

  // @LINE:26
  class ReverseEnderecoController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:30
    def deleteEndereco: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.EnderecoController.deleteEndereco",
      """
        function(id0) {
          return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/deleteEndereco/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:26
    def enderecosPessoaFisica: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.EnderecoController.enderecosPessoaFisica",
      """
        function(idPessoaFisica0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/enderecosPessoaFisica/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("idPessoaFisica", idPessoaFisica0))})
        }
      """
    )
  
    // @LINE:27
    def addEndereco: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.EnderecoController.addEndereco",
      """
        function(idPessoaFisica0) {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/addEndereco/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("idPessoaFisica", idPessoaFisica0))})
        }
      """
    )
  
    // @LINE:28
    def endereco: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.EnderecoController.endereco",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/endereco/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:29
    def updateEndereco: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.EnderecoController.updateEndereco",
      """
        function(id0) {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/updateEndereco/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
  }

  // @LINE:6
  class ReverseFrontendController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:39
    def assetOrDefault: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.FrontendController.assetOrDefault",
      """
        function(file0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + (""" + implicitly[play.api.mvc.PathBindable[String]].javascriptUnbind + """)("file", file0)})
        }
      """
    )
  
    // @LINE:6
    def index: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.FrontendController.index",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + """"})
        }
      """
    )
  
  }

  // @LINE:13
  class ReversePessoaFisicaController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:15
    def pessoaFisica: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.PessoaFisicaController.pessoaFisica",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/pessoaFisica/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:17
    def updatePessoaFisica: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.PessoaFisicaController.updatePessoaFisica",
      """
        function(id0) {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/updatePessoaFisica/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:13
    def addPessoaFisica: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.PessoaFisicaController.addPessoaFisica",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/addPessoaFisica"})
        }
      """
    )
  
    // @LINE:14
    def allPessoasFisicas: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.PessoaFisicaController.allPessoasFisicas",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/allPessoasFisicas"})
        }
      """
    )
  
    // @LINE:18
    def deletePessoaFisica: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.PessoaFisicaController.deletePessoaFisica",
      """
        function(id0) {
          return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/deletePessoaFisica/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
  }


}
