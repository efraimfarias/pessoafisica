// @GENERATOR:play-routes-compiler
// @SOURCE:/home/efraimfarias/samaiait/conf/routes
// @DATE:Sat Jul 06 22:44:17 BRT 2019

import play.api.mvc.Call


import _root_.controllers.Assets.Asset
import _root_.play.libs.F

// @LINE:6
package controllers {

  // @LINE:26
  class ReverseEnderecoController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:30
    def deleteEndereco(id:Long): Call = {
      
      Call("DELETE", _prefix + { _defaultPrefix } + "api/deleteEndereco/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("id", id)))
    }
  
    // @LINE:26
    def enderecosPessoaFisica(idPessoaFisica:Long): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/enderecosPessoaFisica/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("idPessoaFisica", idPessoaFisica)))
    }
  
    // @LINE:27
    def addEndereco(idPessoaFisica:Long): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "api/addEndereco/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("idPessoaFisica", idPessoaFisica)))
    }
  
    // @LINE:28
    def endereco(id:Long): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/endereco/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("id", id)))
    }
  
    // @LINE:29
    def updateEndereco(id:Long): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "api/updateEndereco/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("id", id)))
    }
  
  }

  // @LINE:6
  class ReverseFrontendController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:39
    def assetOrDefault(file:String): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + implicitly[play.api.mvc.PathBindable[String]].unbind("file", file))
    }
  
    // @LINE:6
    def index(): Call = {
      
      Call("GET", _prefix)
    }
  
  }

  // @LINE:13
  class ReversePessoaFisicaController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:15
    def pessoaFisica(id:Long): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/pessoaFisica/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("id", id)))
    }
  
    // @LINE:17
    def updatePessoaFisica(id:Long): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "api/updatePessoaFisica/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("id", id)))
    }
  
    // @LINE:13
    def addPessoaFisica(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "api/addPessoaFisica")
    }
  
    // @LINE:14
    def allPessoasFisicas(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/allPessoasFisicas")
    }
  
    // @LINE:18
    def deletePessoaFisica(id:Long): Call = {
      
      Call("DELETE", _prefix + { _defaultPrefix } + "api/deletePessoaFisica/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("id", id)))
    }
  
  }


}
