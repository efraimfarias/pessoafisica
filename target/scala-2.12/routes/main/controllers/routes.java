// @GENERATOR:play-routes-compiler
// @SOURCE:/home/efraimfarias/samaiait/conf/routes
// @DATE:Sat Jul 06 22:44:17 BRT 2019

package controllers;

import router.RoutesPrefix;

public class routes {
  
  public static final controllers.ReverseEnderecoController EnderecoController = new controllers.ReverseEnderecoController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseFrontendController FrontendController = new controllers.ReverseFrontendController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReversePessoaFisicaController PessoaFisicaController = new controllers.ReversePessoaFisicaController(RoutesPrefix.byNamePrefix());

  public static class javascript {
    
    public static final controllers.javascript.ReverseEnderecoController EnderecoController = new controllers.javascript.ReverseEnderecoController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseFrontendController FrontendController = new controllers.javascript.ReverseFrontendController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReversePessoaFisicaController PessoaFisicaController = new controllers.javascript.ReversePessoaFisicaController(RoutesPrefix.byNamePrefix());
  }

}
