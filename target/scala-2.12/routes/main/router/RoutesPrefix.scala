// @GENERATOR:play-routes-compiler
// @SOURCE:/home/efraimfarias/samaiait/conf/routes
// @DATE:Sat Jul 06 22:44:17 BRT 2019


package router {
  object RoutesPrefix {
    private var _prefix: String = "/"
    def setPrefix(p: String): Unit = {
      _prefix = p
    }
    def prefix: String = _prefix
    val byNamePrefix: Function0[String] = { () => prefix }
  }
}
