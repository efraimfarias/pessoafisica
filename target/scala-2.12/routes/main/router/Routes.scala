// @GENERATOR:play-routes-compiler
// @SOURCE:/home/efraimfarias/samaiait/conf/routes
// @DATE:Sat Jul 06 22:44:17 BRT 2019

package router

import play.core.routing._
import play.core.routing.HandlerInvokerFactory._

import play.api.mvc._

import _root_.controllers.Assets.Asset
import _root_.play.libs.F

class Routes(
  override val errorHandler: play.api.http.HttpErrorHandler, 
  // @LINE:6
  FrontendController_1: controllers.FrontendController,
  // @LINE:13
  PessoaFisicaController_0: controllers.PessoaFisicaController,
  // @LINE:26
  EnderecoController_2: controllers.EnderecoController,
  val prefix: String
) extends GeneratedRouter {

   @javax.inject.Inject()
   def this(errorHandler: play.api.http.HttpErrorHandler,
    // @LINE:6
    FrontendController_1: controllers.FrontendController,
    // @LINE:13
    PessoaFisicaController_0: controllers.PessoaFisicaController,
    // @LINE:26
    EnderecoController_2: controllers.EnderecoController
  ) = this(errorHandler, FrontendController_1, PessoaFisicaController_0, EnderecoController_2, "/")

  def withPrefix(addPrefix: String): Routes = {
    val prefix = play.api.routing.Router.concatPrefix(addPrefix, this.prefix)
    router.RoutesPrefix.setPrefix(prefix)
    new Routes(errorHandler, FrontendController_1, PessoaFisicaController_0, EnderecoController_2, prefix)
  }

  private[this] val defaultPrefix: String = {
    if (this.prefix.endsWith("/")) "" else "/"
  }

  def documentation = List(
    ("""GET""", this.prefix, """controllers.FrontendController.index()"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/addPessoaFisica""", """controllers.PessoaFisicaController.addPessoaFisica"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/allPessoasFisicas""", """controllers.PessoaFisicaController.allPessoasFisicas"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/pessoaFisica/""" + "$" + """id<[^/]+>""", """controllers.PessoaFisicaController.pessoaFisica(id:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/updatePessoaFisica/""" + "$" + """id<[^/]+>""", """controllers.PessoaFisicaController.updatePessoaFisica(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/deletePessoaFisica/""" + "$" + """id<[^/]+>""", """controllers.PessoaFisicaController.deletePessoaFisica(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/enderecosPessoaFisica/""" + "$" + """idPessoaFisica<[^/]+>""", """controllers.EnderecoController.enderecosPessoaFisica(idPessoaFisica:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/addEndereco/""" + "$" + """idPessoaFisica<[^/]+>""", """controllers.EnderecoController.addEndereco(idPessoaFisica:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/endereco/""" + "$" + """id<[^/]+>""", """controllers.EnderecoController.endereco(id:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/updateEndereco/""" + "$" + """id<[^/]+>""", """controllers.EnderecoController.updateEndereco(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/deleteEndereco/""" + "$" + """id<[^/]+>""", """controllers.EnderecoController.deleteEndereco(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """""" + "$" + """file<.+>""", """controllers.FrontendController.assetOrDefault(file:String)"""),
    Nil
  ).foldLeft(List.empty[(String,String,String)]) { (s,e) => e.asInstanceOf[Any] match {
    case r @ (_,_,_) => s :+ r.asInstanceOf[(String,String,String)]
    case l => s ++ l.asInstanceOf[List[(String,String,String)]]
  }}


  // @LINE:6
  private[this] lazy val controllers_FrontendController_index0_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix)))
  )
  private[this] lazy val controllers_FrontendController_index0_invoker = createInvoker(
    FrontendController_1.index(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.FrontendController",
      "index",
      Nil,
      "GET",
      this.prefix + """""",
      """ Serve index page from public directory""",
      Seq()
    )
  )

  // @LINE:13
  private[this] lazy val controllers_PessoaFisicaController_addPessoaFisica1_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/addPessoaFisica")))
  )
  private[this] lazy val controllers_PessoaFisicaController_addPessoaFisica1_invoker = createInvoker(
    PessoaFisicaController_0.addPessoaFisica,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.PessoaFisicaController",
      "addPessoaFisica",
      Nil,
      "POST",
      this.prefix + """api/addPessoaFisica""",
      """ ############################################################################################ #
 -------------------------------------------------------------------------------------------- #
                                 PESSOA FISICA                                                #
 -------------------------------------------------------------------------------------------- #
 ############################################################################################ #""",
      Seq()
    )
  )

  // @LINE:14
  private[this] lazy val controllers_PessoaFisicaController_allPessoasFisicas2_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/allPessoasFisicas")))
  )
  private[this] lazy val controllers_PessoaFisicaController_allPessoasFisicas2_invoker = createInvoker(
    PessoaFisicaController_0.allPessoasFisicas,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.PessoaFisicaController",
      "allPessoasFisicas",
      Nil,
      "GET",
      this.prefix + """api/allPessoasFisicas""",
      """""",
      Seq()
    )
  )

  // @LINE:15
  private[this] lazy val controllers_PessoaFisicaController_pessoaFisica3_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/pessoaFisica/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_PessoaFisicaController_pessoaFisica3_invoker = createInvoker(
    PessoaFisicaController_0.pessoaFisica(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.PessoaFisicaController",
      "pessoaFisica",
      Seq(classOf[Long]),
      "GET",
      this.prefix + """api/pessoaFisica/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:17
  private[this] lazy val controllers_PessoaFisicaController_updatePessoaFisica4_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/updatePessoaFisica/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_PessoaFisicaController_updatePessoaFisica4_invoker = createInvoker(
    PessoaFisicaController_0.updatePessoaFisica(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.PessoaFisicaController",
      "updatePessoaFisica",
      Seq(classOf[Long]),
      "POST",
      this.prefix + """api/updatePessoaFisica/""" + "$" + """id<[^/]+>""",
      """ PUT     /api/updatePessoaFisica/:id controllers.PessoaFisicaController.updatePessoaFisica(id:Long)""",
      Seq()
    )
  )

  // @LINE:18
  private[this] lazy val controllers_PessoaFisicaController_deletePessoaFisica5_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/deletePessoaFisica/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_PessoaFisicaController_deletePessoaFisica5_invoker = createInvoker(
    PessoaFisicaController_0.deletePessoaFisica(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.PessoaFisicaController",
      "deletePessoaFisica",
      Seq(classOf[Long]),
      "DELETE",
      this.prefix + """api/deletePessoaFisica/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:26
  private[this] lazy val controllers_EnderecoController_enderecosPessoaFisica6_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/enderecosPessoaFisica/"), DynamicPart("idPessoaFisica", """[^/]+""",true)))
  )
  private[this] lazy val controllers_EnderecoController_enderecosPessoaFisica6_invoker = createInvoker(
    EnderecoController_2.enderecosPessoaFisica(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.EnderecoController",
      "enderecosPessoaFisica",
      Seq(classOf[Long]),
      "GET",
      this.prefix + """api/enderecosPessoaFisica/""" + "$" + """idPessoaFisica<[^/]+>""",
      """ ############################################################################################ #
 -------------------------------------------------------------------------------------------- #
                                    ENDERECO                                                  #
 -------------------------------------------------------------------------------------------- #
 ############################################################################################ #""",
      Seq()
    )
  )

  // @LINE:27
  private[this] lazy val controllers_EnderecoController_addEndereco7_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/addEndereco/"), DynamicPart("idPessoaFisica", """[^/]+""",true)))
  )
  private[this] lazy val controllers_EnderecoController_addEndereco7_invoker = createInvoker(
    EnderecoController_2.addEndereco(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.EnderecoController",
      "addEndereco",
      Seq(classOf[Long]),
      "POST",
      this.prefix + """api/addEndereco/""" + "$" + """idPessoaFisica<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:28
  private[this] lazy val controllers_EnderecoController_endereco8_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/endereco/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_EnderecoController_endereco8_invoker = createInvoker(
    EnderecoController_2.endereco(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.EnderecoController",
      "endereco",
      Seq(classOf[Long]),
      "GET",
      this.prefix + """api/endereco/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:29
  private[this] lazy val controllers_EnderecoController_updateEndereco9_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/updateEndereco/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_EnderecoController_updateEndereco9_invoker = createInvoker(
    EnderecoController_2.updateEndereco(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.EnderecoController",
      "updateEndereco",
      Seq(classOf[Long]),
      "POST",
      this.prefix + """api/updateEndereco/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:30
  private[this] lazy val controllers_EnderecoController_deleteEndereco10_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/deleteEndereco/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_EnderecoController_deleteEndereco10_invoker = createInvoker(
    EnderecoController_2.deleteEndereco(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.EnderecoController",
      "deleteEndereco",
      Seq(classOf[Long]),
      "DELETE",
      this.prefix + """api/deleteEndereco/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:39
  private[this] lazy val controllers_FrontendController_assetOrDefault11_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), DynamicPart("file", """.+""",false)))
  )
  private[this] lazy val controllers_FrontendController_assetOrDefault11_invoker = createInvoker(
    FrontendController_1.assetOrDefault(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.FrontendController",
      "assetOrDefault",
      Seq(classOf[String]),
      "GET",
      this.prefix + """""" + "$" + """file<.+>""",
      """###################################################################################
 ----------------------------------------------------------------------------------
 ESSA PORCARIA DE ROTA TEM QUE ESTAR POR ULTIMO  --- IMPORTANT ---- Efra
 ----------------------------------------------------------------------------------
####################################################################################
 Serve static assets under public directory""",
      Seq()
    )
  )


  def routes: PartialFunction[RequestHeader, Handler] = {
  
    // @LINE:6
    case controllers_FrontendController_index0_route(params@_) =>
      call { 
        controllers_FrontendController_index0_invoker.call(FrontendController_1.index())
      }
  
    // @LINE:13
    case controllers_PessoaFisicaController_addPessoaFisica1_route(params@_) =>
      call { 
        controllers_PessoaFisicaController_addPessoaFisica1_invoker.call(PessoaFisicaController_0.addPessoaFisica)
      }
  
    // @LINE:14
    case controllers_PessoaFisicaController_allPessoasFisicas2_route(params@_) =>
      call { 
        controllers_PessoaFisicaController_allPessoasFisicas2_invoker.call(PessoaFisicaController_0.allPessoasFisicas)
      }
  
    // @LINE:15
    case controllers_PessoaFisicaController_pessoaFisica3_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_PessoaFisicaController_pessoaFisica3_invoker.call(PessoaFisicaController_0.pessoaFisica(id))
      }
  
    // @LINE:17
    case controllers_PessoaFisicaController_updatePessoaFisica4_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_PessoaFisicaController_updatePessoaFisica4_invoker.call(PessoaFisicaController_0.updatePessoaFisica(id))
      }
  
    // @LINE:18
    case controllers_PessoaFisicaController_deletePessoaFisica5_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_PessoaFisicaController_deletePessoaFisica5_invoker.call(PessoaFisicaController_0.deletePessoaFisica(id))
      }
  
    // @LINE:26
    case controllers_EnderecoController_enderecosPessoaFisica6_route(params@_) =>
      call(params.fromPath[Long]("idPessoaFisica", None)) { (idPessoaFisica) =>
        controllers_EnderecoController_enderecosPessoaFisica6_invoker.call(EnderecoController_2.enderecosPessoaFisica(idPessoaFisica))
      }
  
    // @LINE:27
    case controllers_EnderecoController_addEndereco7_route(params@_) =>
      call(params.fromPath[Long]("idPessoaFisica", None)) { (idPessoaFisica) =>
        controllers_EnderecoController_addEndereco7_invoker.call(EnderecoController_2.addEndereco(idPessoaFisica))
      }
  
    // @LINE:28
    case controllers_EnderecoController_endereco8_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_EnderecoController_endereco8_invoker.call(EnderecoController_2.endereco(id))
      }
  
    // @LINE:29
    case controllers_EnderecoController_updateEndereco9_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_EnderecoController_updateEndereco9_invoker.call(EnderecoController_2.updateEndereco(id))
      }
  
    // @LINE:30
    case controllers_EnderecoController_deleteEndereco10_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_EnderecoController_deleteEndereco10_invoker.call(EnderecoController_2.deleteEndereco(id))
      }
  
    // @LINE:39
    case controllers_FrontendController_assetOrDefault11_route(params@_) =>
      call(params.fromPath[String]("file", None)) { (file) =>
        controllers_FrontendController_assetOrDefault11_invoker.call(FrontendController_1.assetOrDefault(file))
      }
  }
}
