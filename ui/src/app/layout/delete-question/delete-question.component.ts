import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-delete-question',
  templateUrl: './delete-question.component.html',
  styleUrls: ['./delete-question.component.scss']
})
export class DeleteQuestionComponent implements OnInit {

  deleted = false;
  moduloName = "";

  constructor(
    public dialogRef: MatDialogRef<DeleteQuestionComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {

        this.moduloName = data.moduloName;

  }

  /** Deleting */
  ok() {

    this.deleted = true;
    this.dialogRef.close(this.deleted);
  }


  ngOnInit() {

  }

}


