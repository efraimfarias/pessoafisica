import { Component, OnInit, ViewChild } from '@angular/core';

import { Router } from '@angular/router';

import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { DetailPessoaFisicaComponent } from '../detail-pessoa-fisica/detail-pessoa-fisica.component';

import { AppService } from './../../../app.service';

import * as moment from 'moment';


@Component({
  selector: 'app-list-pessoa-fisica',
  templateUrl: './list-pessoa-fisica.component.html',
  styleUrls: ['./list-pessoa-fisica.component.scss']
})
export class ListPessoaFisicaComponent implements OnInit {


  displayedColumns = ['nome', 'cpf', 'email', 'data_nascimento'];
  dataSource: MatTableDataSource<PessoaFisicaData>;

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  pessoasFisicas: PessoaFisicaData[] = [];

  constructor(public dialog: MatDialog, private appService: AppService, private router: Router) {
  }

  //Formatando a vizualização da data
  formatando(date) {
    if (date != null) {
      return moment(date).format('L');
    }
  }



  ngOnInit() {

    moment.locale("pt-br");

    this.appService.allPessoasFisicas().subscribe((data: any) => {

      this.pessoasFisicas = data;

      this.dataSource = new MatTableDataSource(this.pessoasFisicas);

      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });

  }

  detailPessoaFisica(id?: number) {
    this.router.navigate(['pessoa-fisica/pessoa-fisica/' + id]);
  }



  /**
   * Set the paginator and sort after the view init since this component will
   * be able to query its view for the initialized paginator and sort.
   */
  ngAfterViewInit() {
    //this.dataSource.paginator = this.paginator;
    //this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

}//END ListContasComponent


export interface PessoaFisicaData {
  email: string;
  nome: string;
  cpf: string;
  data_nascimento: string;

}
