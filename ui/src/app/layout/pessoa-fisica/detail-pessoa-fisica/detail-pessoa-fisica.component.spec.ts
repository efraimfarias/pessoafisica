import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailPessoaFisicaComponent } from './detail-pessoa-fisica.component';

describe('DetailPessoaFisicaComponent', () => {
  let component: DetailPessoaFisicaComponent;
  let fixture: ComponentFixture<DetailPessoaFisicaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailPessoaFisicaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailPessoaFisicaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
