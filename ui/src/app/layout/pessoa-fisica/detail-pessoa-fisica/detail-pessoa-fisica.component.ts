import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MatSnackBar } from '@angular/material';

import { MatDatepickerInputEvent, MatDatepickerInput } from '@angular/material/datepicker';

import { FormControl } from '@angular/forms';
import { NgForm, FormGroup } from '@angular/forms';

import * as moment from 'moment';

import { DeleteQuestionComponent } from './../../delete-question/delete-question.component';

import { AppService } from './../../../app.service';

import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatChipInputEvent } from '@angular/material/chips';


@Component({
  selector: 'app-detail-pessoa-fisica',
  templateUrl: './detail-pessoa-fisica.component.html',
  styleUrls: ['./detail-pessoa-fisica.component.scss']
})
export class DetailPessoaFisicaComponent implements OnInit {

  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  telefones: string[] = [];

  @ViewChild('formP', { static: false }) formP: NgForm;

  data_nascimento: any;
  data_cadastro: any;

  idPessoaFisica: any;
  pessoaFisica: PessoaFisica;

  mask9 = ['(', /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]

  constructor(public dialog: MatDialog, private route: ActivatedRoute, private router: Router, private appService: AppService,
    public snackBar: MatSnackBar, public dialogDelete: MatDialog) {

    this.pessoaFisica = new PessoaFisica();

  }//End constructor

  ngOnInit() {

    moment.locale("pt-br");

    const id = +this.route.snapshot.paramMap.get('idPessoaFisica');

    this.idPessoaFisica = id;

    this.data_nascimento = new FormControl("");

    //Update Pessoa Fisica 
    if (this.idPessoaFisica != -1) {

      this.appService.pessoaFisica(this.idPessoaFisica).subscribe((data: any) => {
        this.pessoaFisica = data;

        this.telefones = this.pessoaFisica.telefones;

        this.data_cadastro = this.formatando(this.pessoaFisica.data_cadastro);

        //Get the Birth value
        if (this.pessoaFisica.data_nascimento != null) {
          this.data_nascimento = new FormControl(new Date(this.pessoaFisica.data_nascimento));
        }
      });//End ngOnInit

    }
  }

  //Para alteração de Data
  update(newDate: MatDatepickerInputEvent<Date>) {

    var birthday = new Date(newDate.value);

    //** IMPORTANT */
    //I need put this in format LONG
    this.pessoaFisica.data_nascimento = birthday.getTime();
    this.formP.form.markAsDirty();

  }

  /************** Add Pessoaa Fisica throught API *******************************/
  addPessoaFisica() {

    this.pessoaFisica.telefones = this.telefones;

    this.appService.addPessoaFisica(this.pessoaFisica).subscribe((data: any) => {

      this.snackBar.open('SUCESSO', 'Pessoa Física Cadastrado', {
        duration: 3000, panelClass: ['snackbar']
      });

      this.formP.form.markAsPristine();

      this.pessoaFisica.id = data;

      this.data_cadastro = this.formatando(this.pessoaFisica.data_cadastro);

      //Get the Birth value
      if (this.pessoaFisica.data_nascimento != null) {
        this.data_nascimento = new FormControl(new Date(this.pessoaFisica.data_nascimento));
      }

      this.router.navigate(['pessoa-fisica/pessoa-fisica/' + data]);
    },
      (error) => {
        console.log(error);

        this.snackBar.open('ERRO', error.error[0].mensagem, {
          duration: 3000, panelClass: ['snackbar']
        });
      }
    );
  }//END Add



  /************ Update Pessoa Fisicathrought API **************************/
  updatePessoaFisica() {

    this.appService.updatePessoaFisica(this.pessoaFisica).subscribe((data: any) => {

      this.snackBar.open('SUCESSO', 'Pessoa Física Alterado', {
        duration: 3000, panelClass: ['snackbar']
      });

      this.formP.form.markAsPristine();

      this.telefones = this.pessoaFisica.telefones;

    },
      (error) => {
        console.log(error);

        this.snackBar.open('ERRO', error.error[0].mensagem, {
          duration: 3000, panelClass: ['snackbar']
        });
      }
    );

  }//END Update


  /************************** Delete Pessoa Fisica *******************************/
  deletePessoaFisica() {

    let dialog = this.dialogDelete.open(DeleteQuestionComponent, {
      width: '300px',

      data: {
        moduloName: "Pessoa Física",
      }

    });

    dialog.afterClosed().subscribe(result => {
      if (result) {

        this.appService.deletePessoaFisica(this.pessoaFisica.id).subscribe((data: any) => {
          this.snackBar.open('SUCESSO', 'Pessoa Física excluído', {
            duration: 3000, panelClass: ['snackbar']
          });

          this.router.navigate(['pessoa-fisica']);
        },
          (error) => {
            console.log(error);

            this.snackBar.open('ERRO', error.error[0].mensagem, {
              duration: 3000, panelClass: ['snackbar']
            });
          }

        );

      }
    });
  }//End Delete Pessoa Fisica

  /***************Formatando a vizualização da data***********************/
  formatando(date) {
    return moment(date).format('L');
  }

  /*************** Adicionado o telefone na lista Chips ***************/
  addTelefone(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    if (!(value.indexOf('_') == 14 || value.indexOf('_') == -1)) {

      this.snackBar.open('ERRO', 'Telefone Inválido', {
        duration: 3000, panelClass: ['snackbar']
      });

      return;
    }

    var newValue = value;

    if (newValue.indexOf('_') == 14) {

      newValue = newValue.replace("_", "").replace("-", "");
      newValue = newValue.substring(0, 9) + "-" + newValue.substring(9, newValue.length);
    }

    if (newValue.indexOf('_') == -1) {

      if ((newValue || '').trim()) {

        // Add  Telefone
        this.telefones.push(newValue.trim());
        this.formP.form.markAsDirty();

      }

      // Reset the input value
      if (input) {
        input.value = '';
      }

    }
  }

  /*************** Remove Telefone ********************************************/
  removeTelefone(telefone: string): void {
    const index = this.telefones.indexOf(telefone);

    if (index >= 0) {
      this.telefones.splice(index, 1);
      this.formP.form.markAsDirty();
    }
  }

}


class PessoaFisica {
  id: number;
  email: string;
  nome: string;
  cpf: string;
  data_nascimento: number;
  data_cadastro: number;
  telefones: string[];
}