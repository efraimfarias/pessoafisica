import { Component, OnInit, ViewChild } from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';

import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { AppService } from './../../../app.service';

import { AddEnderecoComponent } from './../add-endereco/add-endereco.component';

@Component({
  selector: 'app-list-endereco',
  templateUrl: './list-endereco.component.html',
  styleUrls: ['./list-endereco.component.scss']
})
export class ListEnderecoComponent implements OnInit {

  displayedColumns = ['logradouro', 'bairro', 'cidade', 'uf', 'cep'];
  dataSource: MatTableDataSource<EnderecoData>;

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  enderecos: EnderecoData[] = [];
  idPessoaFisica: any;

  constructor(private route: ActivatedRoute, public dialog: MatDialog, private appService: AppService, private router: Router) {
  }


  ngOnInit() {

    this.idPessoaFisica = +this.route.snapshot.paramMap.get('idPessoaFisica');

    this.appService.enderecosPessoaFisica(this.idPessoaFisica).subscribe((data: any) => {

      this.enderecos = data;

      this.dataSource = new MatTableDataSource(this.enderecos);

      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });

  }

  /***************** Call the dialog for ADD Contas *******************/
  addUpEndereco(id ?: number): void {
    let dialogRef = this.dialog.open(AddEnderecoComponent, {
      width: '650px',
      panelClass: 'windowData',
      data: {
        idPessoaFisica: this.idPessoaFisica,
        idEndereco: id
      }

    });

    dialogRef.afterClosed().subscribe(result => {

      //Efra só chamar se realmente salvou
      //Será que é a maneira certa?
      this.ngOnInit();

    });
  }//End addUpConta


  /**
   * Set the paginator and sort after the view init since this component will
   * be able to query its view for the initialized paginator and sort.
   */
  ngAfterViewInit() {
    //this.dataSource.paginator = this.paginator;
    //this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

}//END ListContasComponent


export interface EnderecoData {
  logradouro: string;
  bairro: string;
  cidade: string;
  uf: string;
  cep: string;
 
}

