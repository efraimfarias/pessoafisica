import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material';
import { MatIconModule} from '@angular/material/icon';
import { MatFormFieldModule} from '@angular/material/form-field';
import { MatTableModule} from '@angular/material/table';
import { MatPaginatorModule} from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatButtonModule} from '@angular/material/button';
import { MatInputModule} from '@angular/material/input';
import { MatToolbarModule} from '@angular/material/toolbar';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSnackBarModule } from '@angular/material/snack-bar';

import {MatChipsModule} from '@angular/material/chips';

import { MAT_DATE_LOCALE } from '@angular/material/core';

import { TextMaskModule } from 'angular2-text-mask';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { PessoaFisicaRoutingModule } from './pessoa-fisica-routing.module';
import { PessoaFisicaComponent } from './pessoa-fisica.component';
import { ListPessoaFisicaComponent } from './list-pessoa-fisica/list-pessoa-fisica.component';
import { DetailPessoaFisicaComponent } from './detail-pessoa-fisica/detail-pessoa-fisica.component';
import { ListEnderecoComponent } from './list-endereco/list-endereco.component';
import { AddEnderecoComponent } from './add-endereco/add-endereco.component';

@NgModule({
  declarations: [PessoaFisicaComponent, ListPessoaFisicaComponent, DetailPessoaFisicaComponent, ListEnderecoComponent, AddEnderecoComponent],
  imports: [
    CommonModule, PessoaFisicaRoutingModule, MatIconModule, MatFormFieldModule, MatDatepickerModule, MatNativeDateModule,
    MatTableModule, MatPaginatorModule, MatButtonModule, MatInputModule, MatToolbarModule,
    ReactiveFormsModule, FormsModule, MatDialogModule, MatSnackBarModule, MatSortModule,
    MatChipsModule, TextMaskModule
  ]
  ,


  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'pt-BR' },
  ],

  entryComponents: [AddEnderecoComponent
  ]
})
export class PessoaFisicaModule { }
