import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PessoaFisicaComponent } from './pessoa-fisica.component';

import {  ListPessoaFisicaComponent } from './list-pessoa-fisica/list-pessoa-fisica.component';
import { DetailPessoaFisicaComponent } from './detail-pessoa-fisica/detail-pessoa-fisica.component';

const routes: Routes = [
    {
        path: '',
        component: PessoaFisicaComponent,
        children: [
            {
                path: '', component: ListPessoaFisicaComponent
            },
            {
                 path: 'pessoa-fisica/:idPessoaFisica', component: DetailPessoaFisicaComponent
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PessoaFisicaRoutingModule {}
