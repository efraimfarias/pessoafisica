import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MatSnackBar } from '@angular/material';

import { NgForm, FormGroup } from '@angular/forms';

import { DeleteQuestionComponent } from './../../delete-question/delete-question.component';

import { AppService } from './../../../app.service';

import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatChipInputEvent } from '@angular/material/chips';

@Component({
  selector: 'app-add-endereco',
  templateUrl: './add-endereco.component.html',
  styleUrls: ['./add-endereco.component.scss']
})
export class AddEnderecoComponent implements OnInit {

  // visible = true;
  // selectable = true;
  // removable = true;
  // addOnBlur = true;
  // readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  // telefones: string[] = [];

  idEndereco: any;
  idPessoaFisica: any;
  endereco: Endereco;

  // mask9 = ['(', /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]

  @ViewChild('formP', { static: false }) formP: NgForm;

  constructor(private router: Router, private appService: AppService,
    public snackBar: MatSnackBar, public dialogDelete: MatDialog,
    public dialogRef: MatDialogRef<AddEnderecoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {

    this.endereco = new Endereco();

    if (data != null) {
      console.log(data);
      this.idEndereco = data.idEndereco;
      this.idPessoaFisica = data.idPessoaFisica;

    }
  }//End constructor


  ngOnInit() {

    //Update Endereco 
    if (this.idEndereco != null) {

      this.appService.endereco(this.idEndereco).subscribe((data: any) => {
        this.endereco = data;

        // this.telefones = this.endereco.telefones;

        console.log(this.endereco);
      });
    }

  }//End ngOnInit


  /************* Cancel button or click out the dialog ****************/
  onNoClick(): void {
    this.dialogRef.close();
  }


  /************** Add Endereco throught API *******************************/
  addEndereco() {

    // this.endereco.telefones = this.telefones;

    this.appService.addEndereco(this.idPessoaFisica, this.endereco).subscribe((data: any) => {

      this.dialogRef.close();

      this.snackBar.open('SUCESSO', 'Endereço Cadastrado', {
        duration: 3000, panelClass: ['snackbar']
      });


    },
      (error) => {

        this.snackBar.open('ERRO', error.error, {
          duration: 3000, panelClass: ['snackbar']
        });
      }
    );
  }//END Add



  /************ Update Endereco throught API **************************/
  updateEndereco() {

    this.appService.updateEndereco(this.endereco).subscribe((data: any) => {

      this.dialogRef.close();

      this.snackBar.open('SUCESSO', 'Endereço Alterado', {
        duration: 3000, panelClass: ['snackbar']
      });


    },
      (error) => {

        this.snackBar.open('ERRO', error.error, {
          duration: 3000, panelClass: ['snackbar']
        });
      }
    );
  }//END Update


  /************************** Delete Endereco *******************************/
  deleteEndereco() {

    let dialog = this.dialogDelete.open(DeleteQuestionComponent, {
      width: '300px',

      data: {
        moduloName: "Endereço",
      }

    });

    dialog.afterClosed().subscribe(result => {
      if (result) {

        this.appService.deleteEndereco(this.endereco.id).subscribe((data: any) => {

          this.dialogRef.close();

          this.snackBar.open('SUCESSO', 'Endereço excluído', {
            duration: 3000, panelClass: ['snackbar']
          });

        },
          (error) => {
            console.log(error);

            this.snackBar.open('ERRO', error.error[0].mensagem, {
              duration: 3000, panelClass: ['snackbar']
            });
          }

        );

      }
    });
  }

  // addTelefone(event: MatChipInputEvent): void {
  //   const input = event.input;
  //   const value = event.value;

  //   //console.log(event);
  //   console.log(value.indexOf('_') == 14 || value.indexOf('_') == -1);

  //   // Add  Telefone

  //   var newValue = value;

  //   if (newValue.indexOf('_') == 14) {

  //     newValue = newValue.replace("_", "").replace("-", "");
  //     newValue = newValue.substring(0, 9) + "-" + newValue.substring(9, newValue.length);
  //   }

  //   if (newValue.indexOf('_') == -1) {

  //     if ((newValue || '').trim()) {

  //       console.log(value.length);

  //       // this.mask9  = ['(', /\d/, /\d/, ')', ' ', /\d/,/\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/]


  //       this.telefones.push(newValue.trim());
  //       this.formP.form.markAsDirty();

  //     }


  //     // Reset the input value
  //     if (input) {
  //       input.value = '';
  //     }

  //   }
  // }

  // removeTelefone(telefone: string): void {
  //   const index = this.telefones.indexOf(telefone);

  //   if (index >= 0) {
  //     this.telefones.splice(index, 1);
  //     this.formP.form.markAsDirty();
  //   }
  // }


  pesquisacep() {

    //Nova variável "cep" somente com dígitos.
    var cep = this.endereco.cep.replace(/\D/g, '');

    //Verifica se campo cep possui valor informado.
    if (cep != "") {

      //Expressão regular para validar o CEP.
      var validacep = /^[0-9]{8}$/;

      //Valida o formato do CEP.
      if (validacep.test(cep)) {

        //Preenche os campos com "..." enquanto consulta webservice.
        this.endereco.logradouro = "....";
        this.endereco.bairro = "....";
        this.endereco.cidade = "....";
        this.endereco.uf = "....";


        this.appService.getCep(cep).subscribe((data: any) => {

          if (!("erro" in data)) {

            console.log(data);
            //Atualiza os campos com os valores.
            this.endereco.logradouro = (data.logradouro);;
            this.endereco.bairro = (data.bairro);
            this.endereco.cidade = (data.localidade);
            this.endereco.uf = (data.uf);

          } else {
            this.snackBar.open('ERRO', 'Cep Não encontrado', {
              duration: 3000, panelClass: ['snackbar']
            });

          }


        });
      } //end if.
      else {
        //cep é inválido.
        this.snackBar.open('ATENÇÂO', 'Cep inválido', {
          duration: 3000, panelClass: ['snackbar']
        });

      }
    } //end if.
    else {
      //cep sem valor, limpa formulário.
      //alert("CEP sem valor");
      this.snackBar.open('ATENÇÂO', 'Cep sem valor', {
        duration: 3000, panelClass: ['snackbar']
      });
    }
  }//END Pesquisa Cep


}




class Endereco {
  id: number;
  logradouro: string;
  bairro: string;
  cidade: string;
  uf: string;
  cep: string;
  // telefones: string[];
}
