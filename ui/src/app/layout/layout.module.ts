import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';


import { MatIconModule} from '@angular/material/icon';


import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { HeaderComponent } from './components/header/header.component';

import {MatDialogModule} from '@angular/material/dialog';
import { MatButtonModule} from '@angular/material/button';
import { MatInputModule} from '@angular/material/input';


import { DeleteQuestionComponent } from './delete-question/delete-question.component';

@NgModule({
    imports: [
        CommonModule,
        LayoutRoutingModule,
        TranslateModule,
        NgbDropdownModule, MatDialogModule, MatIconModule, MatButtonModule, MatInputModule
    ],
    declarations: [LayoutComponent, 
        HeaderComponent, DeleteQuestionComponent ]

    ,

    entryComponents: [ DeleteQuestionComponent  ]
})
export class LayoutModule { }
