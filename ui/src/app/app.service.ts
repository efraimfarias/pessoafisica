import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

 //import 'rxjs/add/operator/map';


const httpOptions = {
  headers: new HttpHeaders(
    { 'Content-Type': 'application/json' ,
    'responseType': 'text' })
};


/***************************************************************************************
 * *************Class representing application service**********************************
 * 
 * ************* COMMUNICATION WITH BACKEND CONTROLLER ********************************* 
 * **********************API INTO PLAYFRAMEWORK*****************************************
 *
 * @class AppService.
 * 
 ***************************************************************************************/
@Injectable()
export class AppService {

  constructor(private http: HttpClient) {
  }


  /************************************************************************
  *************************** PESSOA FISICA********************************
  ************************************************************************/

  /************************* Todas as Pessoas Físicas  *******************/
  public allPessoasFisicas() {
    return this.http.get('/api/allPessoasFisicas')
      .pipe(response => response);
  }

  /************************* Add Pessoa Fisica*****************************/
  public addPessoaFisica(pessoaFisica: any) {
    return this.http.post('/api/addPessoaFisica', JSON.stringify(pessoaFisica), httpOptions)
      .pipe(
        response => response
      );
  }

  /************************* Update Pessoa Fisica*****************************/
  public updatePessoaFisica(pessoaFisica: any) {
    return this.http.post('/api/updatePessoaFisica/' + pessoaFisica.id, JSON.stringify(pessoaFisica), httpOptions)
      .pipe(
        response => response
      );
  }

  /************************* Get Pessoa Fisica by id ****************************/
  public pessoaFisica(id: number) {
    return this.http.get('/api/pessoaFisica/' + id)
      .pipe(response => response);
  }


  /**************************** Delete Pessoa Fisica ******************************/
  public deletePessoaFisica(idPessoaFisica: number) {
    return this.http.delete('/api/deletePessoaFisica/' + idPessoaFisica)
      .pipe(response => response);
  }


  /************************************************************************
  *************************** ENDERECO  ***********************************
  ************************************************************************/

  /******************** Todos enderecos de uma Pessoa Fisica ****************/
  public enderecosPessoaFisica(idPessoFisica: number) {

    return this.http.get('/api/enderecosPessoaFisica/' + idPessoFisica)
      .pipe(response => response);
  }

  /************************* Add Endereço *********************************/
  public addEndereco(idPessoaFisica: number, endereco: any) {
    return this.http.post('/api/addEndereco/'+idPessoaFisica, JSON.stringify(endereco), httpOptions)
      .pipe(
         response => response
      );
  }

  /************************* Get Endereco by id ****************************/
  public endereco(idEndereco: number) {
    return this.http.get('/api/endereco/' + idEndereco)
      .pipe(response => response);
  }

   /************************* Update Endereco *******************************/
   public updateEndereco(endereco: any) {
    return this.http.post('/api/updateEndereco/' + endereco.id, JSON.stringify(endereco), httpOptions)
      .pipe(
        response => response
      );
  }


  /**************************** Delete Endereco ******************************/
  public deleteEndereco(idEndereco: number) {
    return this.http.delete('/api/deleteEndereco/' + idEndereco)
      .pipe(response => response);
  }


  /*********** CEP ***********************************************************/
  public getCep(cep: String) {
    return this.http.get('https://viacep.com.br/ws/' + cep + '/json/')
      .pipe(response => response);
  }



}



