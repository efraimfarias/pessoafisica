﻿CREATE SEQUENCE public.endereco_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 3
  CACHE 1;
ALTER TABLE public.endereco_id_seq
  OWNER TO samaiait;



CREATE SEQUENCE public.pessoa_fisica_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 6
  CACHE 1;
ALTER TABLE public.pessoa_fisica_id_seq
  OWNER TO samaiait;




CREATE TABLE public.pessoa_fisica
(
  id bigint NOT NULL DEFAULT nextval('pessoa_fisica_id_seq'::regclass),
  cpf bigint,
  nome character varying(255),
  email character varying(255),
  data_nascimento timestamp with time zone,
  data_cadastro timestamp with time zone,
  telefones character varying[],
  CONSTRAINT pk_pessoa_fisica PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.pessoa_fisica
  OWNER TO samaiait;




CREATE TABLE public.endereco
(
  id bigint NOT NULL DEFAULT nextval('endereco_id_seq'::regclass),
  pessoa_fisica_id bigint NOT NULL,
  cep character varying(255),
  logradouro character varying(255),
  bairro character varying(255),
  cidade character varying(255),
  uf character varying(255),
  CONSTRAINT pk_endereco PRIMARY KEY (id),
  CONSTRAINT fk_endereco_pessoa_fisica_id FOREIGN KEY (pessoa_fisica_id)
      REFERENCES public.pessoa_fisica (id) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.endereco
  OWNER TO samaiait;

CREATE INDEX ix_endereco_pessoa_fisica_id
  ON public.endereco
  USING btree
  (pessoa_fisica_id);
