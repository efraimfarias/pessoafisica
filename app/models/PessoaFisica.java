package models;

import java.util.*;
import javax.persistence.*;

import io.ebean.annotation.DbArray;

import io.ebean.*;
import play.data.format.*;
import play.data.validation.*;


@Entity
public class PessoaFisica extends Model {

    @Id
    @Constraints.Min(10)
    public Long id;

    @Constraints.Required
    public Long cpf;

    @Constraints.Required
    public String nome;

    @Constraints.Required
    public String email;

    @Formats.DateTime(pattern="dd/MM/yyyy")
    public Date data_nascimento;

    @Formats.DateTime(pattern="dd/MM/yyyy")
    public Date data_cadastro = new Date();

    @DbArray
    public List<String> telefones = new ArrayList<String>();

    public static final Finder<Long, PessoaFisica> find = new Finder<>(PessoaFisica.class);

    @OneToMany(cascade = CascadeType.ALL)
    public List<Endereco> enderecos;

}