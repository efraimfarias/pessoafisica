package models;

import java.util.*;
import javax.persistence.*;

import io.ebean.*;
import io.ebean.annotation.DbArray;
import play.data.format.*;
import play.data.validation.*;


@Entity
public class Endereco extends Model {

    @Id
    @Constraints.Min(10)
    public Long id;

    public String cep;
    public String logradouro;
    public String bairro;
    public String cidade;
    public String uf;

    // @DbArray
    // public List<String> telefones = new ArrayList<String>();

    public static final Finder<Long, Endereco> find = new Finder<>(Endereco.class);
}