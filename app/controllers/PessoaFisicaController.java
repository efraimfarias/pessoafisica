package controllers;

/**
 * Implementação de um api para Cadastro de Pessoa Física (simples).
 * 
 * Apenas o CRUD de Pessoa Física 
 * 
 * Todas as rotas foram mapeadas no arquivo routes.conf
 * 
 * O model é models/PessoaFisica.java
 *   
 * @author efraimfarias 
 * @date 05/07/2019
 * @version 1.0
 * 
 * 
 */

import java.text.SimpleDateFormat;
import java.text.ParseException;

import java.util.Date;
import java.util.List;
import java.util.ArrayList; 

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import org.json.simple.JSONObject;
import org.json.simple.JSONArray; 

import models.PessoaFisica;
import play.libs.Json;
import play.mvc.*;

import io.ebean.*;

/*
 * Classe principal que interage com todas as chamadas via REST
 * Criando regras de validação de dados e CRUD na Base de Dados
 *
 */
public class PessoaFisicaController extends Controller {

    /*
     * Onde será armazenado uma lista de Pre condições
     */
    ArrayList<JSONObject> preCondFails = new ArrayList<JSONObject>();
          
  
    /*
     * Todos as Pessoas Físicas no sistema
     * 
     * Este método é usado para retornar todas as pessoas físicas
     *   
     * @return OK: json de Pessoas Físicas
     * 
     */
    public Result allPessoasFisicas() {

        //Buscando todas as Pessoas Físicas 
        List<PessoaFisica> pessoasFisicas = PessoaFisica.find.all();

        JsonNode jsonNode = Json.toJson(pessoasFisicas);
        return ok(jsonNode).as("application/json");
    }


    /*
     * Criando uma Pessoa Física
     * 
     * Este método é usado para criar uma Pessoa Física através de dados que serão validados
     * 
     * Os campos de preenchimentos são passados atráves do Body Json 
     *   
     * @return status OK: criado ou com ressalvas nas pre condições
     * 
     */
    @BodyParser.Of(BodyParser.Json.class)
    public Result addPessoaFisica() {

        //obtendo o body via json
        JsonNode json = request().body().asJson();
                    
        long cpf = json.findPath("cpf").asLong();
        String nome = json.findPath("nome").textValue();
        String email = json.findPath("email").textValue();
        long data_nascimento = json.findPath("data_nascimento").asLong();

        List<String> telefones = new ArrayList<String>();
        for (JsonNode node : json.findPath("telefones")) {
            telefones.add(node.textValue());
        }

        //Verificando as precondições. -1, pois não quero encontrar nenhum ID - cadastro
        validaDados(-1, nome, cpf, email, data_nascimento);

        //Se existe é por que existe ao menos uma condição não satisfatório, retornando a falha
        if(preCondFails.size() > 0){
            JsonNode fails = Json.toJson(preCondFails);
            preCondFails = new ArrayList<JSONObject>();
            return status(412, fails);
        }

        //Criando uma Pessoa Física e atribuindo os valores validados
        PessoaFisica pessoaFisica = new PessoaFisica();
        pessoaFisica.cpf = cpf;
        pessoaFisica.nome = nome;
        pessoaFisica.email = email;
        pessoaFisica.telefones = telefones;

        if(data_nascimento != 0) {
            pessoaFisica.data_nascimento = new Date(data_nascimento);
        }

        //Data de hoje
        pessoaFisica.data_cadastro = new Date();

        //Salvando o Registro de Pessoa Física
        pessoaFisica.save();

        //Tudo certo
        JsonNode jsonNode = Json.toJson(pessoaFisica.id);
        return ok(jsonNode).as("application/json");
        
    }
    

    /*
     * Alterando uma Pessoa Fisica
     * 
     * Este método é usado para alterar uma Pessoa Fisica
     * Os campos preeenchimentos(alterados) são passados atráves de Body Json 
     * 
     * @param long id O identificador da Pessoa Fisica
     *   
     * @return status OK: alterado ou com ressalvas nas pre condições
     * 
     */
    @BodyParser.Of(BodyParser.Json.class)
    public Result updatePessoaFisica(long id) {

        //Verificando se o Pessoa Fisica existe com o Id informado
        if( PessoaFisica.find.byId(id) != null ) {

            //Obtendo o Pessoa Fisica atraves do id
            PessoaFisica pessoaFisica = PessoaFisica.find.byId(id);

            //obtendo o body via json
            JsonNode json = request().body().asJson();
        
            long cpf = json.findPath("cpf").asLong();
            String nome = json.findPath("nome").textValue();
            String email = json.findPath("email").textValue();
            long data_nascimento = json.findPath("data_nascimento").asLong();

            List<String> telefones = new ArrayList<String>();
            for (JsonNode node : json.findPath("telefones")) {
                telefones.add(node.textValue());
            }

            //Verificando as precondições
            validaDados(id, nome, cpf, email, data_nascimento);

            //Se existe é por que existe ao menos uma condição não satisfatório, retornando a falha
            if(preCondFails.size() > 0){
                JsonNode fails = Json.toJson(preCondFails);
                preCondFails = new ArrayList<JSONObject>();
                return status(412, fails);
            }
 
            //Atribuindo os novos valores validados
            pessoaFisica.cpf = cpf;
            pessoaFisica.nome = nome;
            pessoaFisica.email = email;
            
            if(data_nascimento != 0) {
                pessoaFisica.data_nascimento = new Date(data_nascimento);
            }
            
            pessoaFisica.telefones = telefones;

            //Alterando uma Pessoa Física
            pessoaFisica.update();

            //Tudo certo
            return status(200);

        } else { //Não existe o ID informado

            //Registrando o erro
            addInvalidos("pessoafisica.id", "inexistente", "A Pessoa Física é inexistente", "");
  
            JsonNode fails = Json.toJson(preCondFails);
            preCondFails = new ArrayList<JSONObject>();
        
            //Falha
            return status(405, fails);
        }
    }



    /*
     * Obtendo uma Pessoa Fisica
     * 
     * Este método é usado para obter uma Pessoa Fisica através do ID  
     * 
     * @param long id O identificador da Pessoa Física
     *   
     * @return status OK: encontrado ou não encontrado 
     * 
     */
    public Result pessoaFisica(long id) {

        //Obtendo a Pessoa Fisica atraves do id
        PessoaFisica pessoaFisica = PessoaFisica.find.byId(id);

        //Se encontrou a Pessoa Fisica
        if( pessoaFisica != null) {
          
            JsonNode jsonNode = Json.toJson(pessoaFisica);
                       
            //Tudo certo
            return status( 200, jsonNode );
            
        } else {

            //ID não encontrado
            return status(404);
        }
    }
    

    /*
     * Deletando uma Pessoa Fisica
     * 
     * Este método é usado para excluir uma Pessoa Fisica através do ID  
     * 
     * @param long id: O identificador da Pessoa Fisica
     *   
     * @return status OK: excluido ou não encontrado ou registro com restrinções  
     * 
     */
    public Result deletePessoaFisica(long id) {

        //status inicial 200. O Valor poderá ser alterado conforme condições de validação
        int statusNumber = 200;

        //Verificando se a Pessoa Fisica existe com o Id informado
        if( PessoaFisica.find.byId(id) != null ){
            
            //Esta vinculado a outros registros
            try {
            
                //Deletando a Pessoa Fisica 
                PessoaFisica.find.ref(id).delete();

                //Deu certo
                return status(statusNumber);
                
            } catch (Exception e) {

                //Houve uma restrição. Poderá estar sendo vinculado à outros registros
                addInvalidos("pessoafisica.vinculo", "vinculado", "A Pessoa Física está vinculado a outra tabela, não é possivel remover", "");

                //Falha. Pre condições
                statusNumber = 412;
            }

        } else {

            //ID não encontrado. Registrando o Erro 
            addInvalidos("pessoafisica.id", "inexistente", "A Pessoa Física é inexistente", "");

            //Falha
            statusNumber = 405;
        }
       
        JsonNode fails = Json.toJson(preCondFails);
        preCondFails = new ArrayList<JSONObject>();
        
        //Deu erro
        return status(statusNumber, fails);
    }
    


    /*
     * Verificando se existe um Cpf
     * 
     * Este método é usado para verificar se existe um cpf de uma outra Pessoa Física  
     * 
     * @param long cpf a ser inserido ou alterado
     * @param long id da Pessoa Fisica, caso seja uma alteração
     *   
     * @return Boolean true ou false  
     * 
     */
    private Boolean existeCpf(long cpf, long id) {

        //String sql para encontrar o cpf 
        String sql = " select pf.cpf from pessoa_fisica pf where pf.cpf = :cpf and pf.id != :id";

        //Adicionando os critérios
        SqlQuery sqlQuery = Ebean.createSqlQuery(sql);
        sqlQuery.setParameter("cpf", cpf);
        sqlQuery.setParameter("id",  id );
       
        List<SqlRow> list = sqlQuery.findList();

        //Se existe é por que encontrou
        if( list.size() > 0 ){
            return true;
        } else {   
            return false;
        }
        
    }

    /*
     * Validando os dados
     * 
     * Este método é usado para validar os dados conforme regra de negócio
     * 
     * A cada pre condição inválida é registrado o erro e então adicionado numa Lista de erros
     * 
     * @param long id da Pessoa Física. Não é usado para cadastro, apenas no caso de alteração
     * @param String nome 
     * @param String cpf
     * @param String email
     * @para long data_nascimento
     *   
     * @return
     * 
     */
    private void validaDados(long id, String nome, Long cpf, String email, long data_nascimento){

        //Nome não pode ser vazio
        if( (nome == "")  || (nome == null) ) {
            addInvalidos("pessoafisica.nome", "obrigatorio", "O nome é obrigatório", "");
        
        } 
        
        //Cpf não pode ser vazio
        if(  (cpf == 0) ) {
            addInvalidos("pessoafisica.cpf", "obrigatorio", "O CPF é obrigatório", "");

        } else if( String.valueOf(cpf).length() != 11) { //Somente números e exatos 11 números
            addInvalidos("pessoafisica.cpf", "invalido", "O CPF deve possuir exatamente 11 caracteres numéricos", "11");

        } else if( !Utils.isValidCPF( String.valueOf(cpf) ) ){
            addInvalidos("pessoafisica.cpf", "invalido", "O CPF é inválido", "");
                                
        } else if( existeCpf( cpf, id ) ) { //Não poderá existir um cpf igual já registtrado. Salvo alteração com o mesmo ID
            addInvalidos("pessoafisica.cpf", "duplicado", "O CPF é duplicado, já existe outra Pessoa Física com o CPF informado", "");

        }

        //Email não pode ser vazio
        if( (email == null ) || (email == "") ){
            addInvalidos("pessoafisica.email", "obrigatorio", "O e-mail é obrigatório", "");
        
        } else if( !email.matches("^(.+)@(.+)$") ) { //Validando o básico de um email
            addInvalidos("pessoafisica.email", "invalido", "O e-mail informado é inválido", "");

        } else if( existeEmail(email, id)) { //Não poderá existir um email igual já registrado. Salvo alteração com o mesmo ID
            addInvalidos("pessoafisica.email", "duplicado", "O e-mail é duplicado, já existe outra Pessoa Física com o e-mail informado", "");
        }

        Date dtNascimento = new Date(data_nascimento);

        //A data de nascimento não pode ser maior que hoje
        if (dtNascimento.after(new Date())) {
            addInvalidos("pessoafisica.dataNascimento", "menorDataAtual", "A data de nascimento deve ser menor que a data atual", "");
         }
    }


    /*
     * Adicionado os erros numa lista 
     * 
     * Este método é usado para registrar os erros encontrados numa Lista
     * 
     * A cada pre condição inválida a chamada desse método é feita
     * 
     * @param String local_erro
     * @param String erro 
     * @param String mensagem
     * @param String arg
     *   
     * @return
     * 
     */
    private void addInvalidos(String local_erro, String erro, String mensagem, String arg){
        JSONObject validationItem = new JSONObject();
        JSONArray args = new JSONArray(); 
        
        validationItem.put("local_erro", local_erro);
        validationItem.put("erro", erro); 
        validationItem.put("mensagem", mensagem);

        //Vetor
        if( arg != "" ){
            args.add(Integer.parseInt(arg) );
        }
        validationItem.put("args", args);

        //Adicinando na lista de erros
        preCondFails.add(validationItem);
    }


    /*
     * Verificando se existe um email
     * 
     * Este método é usado para verificar se existe um email de uma outra Pessoa Fisica  
     * 
     * @param String email a ser inserido ou alterado
     * @param long id da Pessoa Física, caso seja uma alteração
     *   
     * @return Boolean true ou false  
     * 
     */
    private Boolean existeEmail(String email, long id) {

        //String sql para encontrar o email
        String sql = " select pf.email from pessoa_fisica pf where pf.email = :email and pf.id != :id";

        SqlQuery sqlQuery = Ebean.createSqlQuery(sql);
        sqlQuery.setParameter("email", email);
        sqlQuery.setParameter("id",  id );
       
        List<SqlRow> list = sqlQuery.findList();

        //Se existe é por que encontrou
        if( list.size() > 0 ){
            return true;
        } else {   
        return false;
        }
    }
    
   
}