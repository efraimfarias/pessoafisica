package controllers;

/**
 * Implementação de um api para Cadastro de Pessoa Física (simples).
 * 
 * CRUD de Endereco de Pessoa Física 
 * 
 * Todas as rotas foram mapeadas no arquivo routes.conf (Endereco)
 * 
 * O model é models/Endereco.java
 *   
 * @author efraimfarias 
 * @date 05/07/2019
 * @version 1.0
 *  
 * 
 */

import java.util.Date;
import java.util.List;
import java.util.ArrayList; 

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import org.json.simple.JSONObject;
import org.json.simple.JSONArray; 

import io.ebean.*;
import models.Endereco;
import models.PessoaFisica;
import play.libs.Json;
import play.mvc.*;

/*
 * Classe secundaria que interage com todas as chamadas via REST
 * Criando regras de validação de dados para Endereco e CRUD na Base de Dados
 *
 */
public class EnderecoController extends Controller {

     /*
     * Todos os Endereços de uma Pessoa Física
     * 
     * Este método é usado para retornar todas os endereços de uma Pessoa Física
     * 
     * @param long id O identificador da Pessoa Fisica
     *   
     * @return OK: json de Enderecos
     * 
     */
    public Result enderecosPessoaFisica(Long idPessoaFisica) {

        //Complexo! Mas é como o Ebean faz os criterios de buscas
        List<Endereco> enderecos = Endereco.find.query().where().and().eq("pessoa_fisica_id", idPessoaFisica).endAnd()
                .findList();

        JsonNode jsonNode = Json.toJson(enderecos);
        return ok(jsonNode).as("application/json");
    }

    /*
     * Criando um novo Endereço
     * 
     * Este método é usado para criar um novo Endereço para uma Pessoa Física através de dados que serão validados
     * 
     * Os campos de preenchimentos são passados atráves do Body Json
     * 
     * @param long id O identificador da Pessoa Fisica
     *   
     * @return status OK: criado ou com ressalvas nas pre condições
     * 
     */
    @BodyParser.Of(BodyParser.Json.class)
    public Result addEndereco(long idPessoaFisica) {

        //obtendo o body via json
        JsonNode json = request().body().asJson();
                    
        String logradouro = json.findPath("logradouro").textValue();
        String bairro = json.findPath("bairro").textValue();
        String cidade = json.findPath("cidade").textValue();
        String uf = json.findPath("uf").textValue();
        String cep = json.findPath("cep").textValue();

        // List<String> telefones = new ArrayList<String>();

        // for (JsonNode node : json.findPath("telefones")) {
        //     telefones.add(node.textValue());

        // }

        //Pelo menos um dos campos deverá estar preenchidos
        if(( 
              ((logradouro == null ) || (logradouro == "")) &&
              ((bairro == null ) || (bairro == "")) &&
              ((cidade == null ) || (cidade == "")) &&
              ((uf == null ) || (uf == "")) &&
              ((cep == null ) || (cep == ""))   
           )) {
            
            return status(412, "Pelo menos um dos campos de Endereço deverá ser preenchido");
        }

        //Criando um Endereco e atribuindo os valores validados
        Endereco endereco = new Endereco();
        endereco.logradouro = logradouro;
        endereco.bairro = bairro;
        endereco.cidade = cidade;
        endereco.uf = uf;
        endereco.cep = cep;
        // endereco.telefones = telefones;

        //Salvando o Registro de Endereco em Pessoa Física
        PessoaFisica pessoaFisica = PessoaFisica.find.byId(idPessoaFisica);
        pessoaFisica.enderecos.add(endereco);
        pessoaFisica.save();
  
        //Tudo certo
        return status(201);
    }


    /*
     * Obtendo um Endereço
     * 
     * Este método é usado para obter um Endereço do ID  
     * 
     * @param long id O identificador do Endereço
     *   
     * @return status OK: encontrado ou não encontrado 
     * 
     */
    public Result endereco(long id) {

        //Obtendo o Endereco atraves do id
        Endereco endereco = Endereco.find.byId(id);

        //Se encontrou o Endereco
        if( endereco != null) {
          
            JsonNode jsonNode = Json.toJson(endereco);
                       
            //Tudo certo
            return status( 200, jsonNode );
            
        } else {

            //ID não encontrado
            return status(404);
        }
    }


     /*
     * Alterando um Endereço
     * 
     * Este método é usado para alterar um Endereço 
     * Os campos preeenchimentos(alterados) são passados atráves de Body Json 
     * 
     * @param long id O identificador de um endereço
     *   
     * @return status OK: alterado ou com ressalvas nas pre condições
     * 
     */
    @BodyParser.Of(BodyParser.Json.class)
    public Result updateEndereco(long id) {

        //Verificando se o Endereco existe com o Id informado
        if( Endereco.find.byId(id) != null ) {

            //Obtendo o Endereço atraves do id
            Endereco endereco = Endereco.find.byId(id);

            //obtendo o body via json
            JsonNode json = request().body().asJson();
                    
            String logradouro = json.findPath("logradouro").textValue();
            String bairro = json.findPath("bairro").textValue();
            String cidade = json.findPath("cidade").textValue();
            String uf = json.findPath("uf").textValue();
            String cep = json.findPath("cep").textValue();
            // List<String> telefones = new ArrayList<String>();

            // for (JsonNode node : json.findPath("telefones")) {
            //     telefones.add(node.textValue());

            // }

            //Pelo menos um dos campos deverá estar preenchidos
            if(( 
                ((logradouro == null ) || (logradouro == "")) &&
                ((bairro == null ) || (bairro == "")) &&
                ((cidade == null ) || (cidade == "")) &&
                ((uf == null ) || (uf == "")) &&
                ((cep == null ) || (cep == ""))   
            )) {
               
            return status(412, "Pelo menos um dos campos de Endereo deverá ser preenchido");
            
            }

 
            //Atribuindo os novos valores validados
            endereco.logradouro = logradouro;
            endereco.bairro = bairro;
            endereco.cidade = cidade;
            endereco.uf = uf;
            endereco.cep = cep;
            // endereco.telefones = telefones;
            
            //Alterando o Endereco
            endereco.update();

            //Tudo certo
            return status(200);

        } else { //Não existe o ID informado

            //Falha
            return status(405, "Não existe o Id Informado");
        }
    }


    /*
     * Deletando um Endereco
     * 
     * Este método é usado para excluir um Endereco através do ID  
     * 
     * @param long id: O identificador de um Endereço
     *   
     * @return status OK: excluido ou não encontrado ou registro com restrinções  
     * 
     */
    public Result deleteEndereco(long id) {

        //status inicial 200. O Valor poderá ser alterado conforme condições de validação
        int statusNumber = 200;
        String msg = "";

        //Verificando se o Endereco existe com o Id informado
        if( Endereco.find.byId(id) != null ){
            
            //Esta vinculado a outros registros
            try {
            
                //Deletando um Endereco 
                Endereco.find.ref(id).delete();

                //Deu certo
                return status(statusNumber);
                
            } catch (Exception e) {

                //Houve uma restrição. Poderá estar sendo vinculado à outros registros
                msg = "O endereço está vinculado, não é possivel remover";

                //Falha. Pre condições
                statusNumber = 412;
            }

        } else {

            //ID não encontrado. Registrando o Erro 
            msg = "O Endereço é inexistente";

            //Falha
            statusNumber = 405;
        }
             
        //Deu erro
       return status(statusNumber, msg);
    }

    
}
