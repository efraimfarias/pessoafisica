# README #
CRUD - o cadastro de uma Pessoa Fisica com os campos: 
Id (auto incremento)
Nome
CPF (com validação de CPF)
E-mail
Data de Nascimento
Data do cadastro (campo interno de sistema preenchido automaticamente com a data atual no momento da inclusão)

Lista de Endereços
- Logradouro
- CEP
- Bairro
- Cidade
- UF

Lista de Telefones
-Telefone (com mascara e validação de 8 e 9 dígitos no telefone)

### How do I get set up? ###

Com o SBT(http://www.scala-sbt.org/download.html) instalado na sua máquina... digite no folder do projeto: sbt run
